# Chat Expresso

## Configuração
O projeto está pronto para rodar dentro da pasta dist/
É sugerido em produção usar a lib "forever" para servir o mesmo.
Tudo que está na raíz e dentro da pasta src é usado em tempo de desenvolvimento. Por convenção a extensão .es6 indica que é fonte, o output sempre será .js (ou seja, não se altera arquivo .js, apenas .es6)
Em tempo de desenvolvimento é necessário rodar na cli a task default do *grunt* para gerar os arquivos para a pasta *dist*.
Deve haver um arquivo *config.js* na pasta *dist*. Há um *config.sample.js* de exemplo na raíz.

### Host (MMPublish que acessa o chat)
	- host: (exemplo: novoexpresso.mmcafe.com.br)
	- porta: (exemplo: 80)

### Database
	- serviceAccount (exemplo: "./service_account_key.json", o caminho da chave gerada pela interface web. Explicação em https://firebase.google.com/docs/server/setup#prerequisites)
    - databaseURL (exemplo: "https://blazing-fire-3775.firebaseio.com/", o caminho da database)
    - endpoint (exemplo: "mmcafecorp", é como se fosse um "subdomínio", possibilitando ter vários "bancos")

Nas *rules* do Firebase deve haver as seguintes segras:

> {
>    "rules": {
>        ".write": "auth.uid === 'mmcafé'",
>        ".read": "auth.uid === 'mmcafé'"
>    }
> }

onde mmcafé é o mesmo valor do uid setado no arquivo

### Server
	- socket_port (exemplo: 8001)
	- api_port (exemplo: 3010)
	- resourcespath (caminho onde serão guardados os arquivos enviados pelos usuários. Pode ser relativo, absoluto, etc... deve checar se tem permissão de escrita no caminho dado. Precisa terminar com /)
	- maxfilesize (exemplo: 5 - é o tamanho, em megaByes, que a API do server suportará para arquivos recebidos)

O chat usa 2 portas: uma para conexões pelo socket e outra para acesso à sua api.

Para rodar o server com o *forever* gerando logs:
> forever start -o out.log -e err.log server.js

## Chat
Toda vez que um client se conecta ao chat, um socket é criado entre ele e o server. Todos eventos emitidos pelo client passam pelo listener 'text' e esse envia para o dispatcher. O socket sempre valida o token enviado pelo client para checar se é um usuário válido.
O chat tem um Broadcaster, que concentra os eventos a serem emitidos para o client.

## API

Quando o serviço é iniciado, uma API é disponibilizada para acesso. Seus endpoints devem ser usados sempre que não for necessária a conexão do socket.
Métodos da API que devam checar o usuário logado devem passar pelo middleware de validação (validate). Esse middleware faz um match entre o id do usuário logado e o token, esses 2 parâmetros são recebidos no header do request.
É necessário que o server tenha imagemagick instalado para fazer a geração de thumbnail.

## Banco de dados
TODO