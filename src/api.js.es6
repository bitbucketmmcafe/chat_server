"use strict";

let config = require('./config.js');

let mkdirp = require('mkdirp'),
    fs = require('fs'),
    getDirName = require('path').dirname,
    easyimg = require('easyimage'),
    Q = require('q');

const readChunk = require('read-chunk'),
    fileType = require('file-type');

let chat = null;

let validate = (req, res, next) => {

    let token = req.headers["token"],
        logged_user_id = req.headers["logged_user_id"];

    chat.validate_token(logged_user_id, token)
        .then(can_access => {

            if (can_access) {
                req.user_id = parseInt(logged_user_id);
                next();
            } else {
                res
                    .status(401)
                    .json({message: "Access forbidden"})
            }
        });
};

module.exports = (app, _chat) => {

    chat = _chat;

    app.get("/socket", (req, res) => {
        console.log("API: socket");
    });

    app.get("/api/config", (req, res) => {
        console.log("API: config");
        res.json({
            socket_port: config.server.socket_port,
            api_port: config.server.api_port,
            maxfilesize: config.server.maxfilesize
        });
    }),

        app.get("/api/rooms/:room_id/history/:history_id", validate, (req, res) => {
            chat
                .find_room_with_user(req.params.room_id, req.user_id)
                .then(room => {
                    room.build_users()
                        .then(() => {
                            room
                                .get_messages(req.params.history_id)
                                .then(messages => {
                                    res.send({room, messages});
                                })
                        })
                })
        }),
        app.get("/api/rooms/:room_id/history/", validate, (req, res) => {
            chat
                .find_room_with_user(req.params.room_id, req.user_id)
                .then(room => {
                    room.build_users()
                        .then(() => {
                            room
                                .get_messages(null)
                                .then(messages => {
                                    res.json({room, messages});
                                })
                        })
                        .fail(error => {
                            console.error(error);
                            res
                                .status(500)
                                .json({message: "Internal server error"})
                        })
                })
                .fail(error => {
                    console.error(error);
                    res
                        .status(401)
                        .json({message: "Access forbidden"})
                })
        }),

        app.get("/api/viewfile/:file_id", (req, res) => {
            chat.filesRef.child(req.params.file_id).once("value", snapshot => {
                let filedata = snapshot.val(),
                    path = `${config.server.resourcespath}${filedata.room_id}/${snapshot.key}`;
                fs.readFile(path, (err, data) => {
                    if (err) {
                        console.error(`API: file ${req.params.file_id} not found`);
                        res.status(404);
                        res.send(null);
                        return;
                    }
                    res.setHeader('Content-type', filedata.filetype);
                    if (filedata.filetype.indexOf("image") < 0 && filedata.filetype.indexOf("pdf") < 0) {
                        res.setHeader("Content-Type", "application/force-download");
                        res.setHeader("Content-Transfer-Encoding", "binary");
                        res.setHeader("Content-Disposition", `attachment; filename="${filedata.filename}"`)
                    }
                    res.send(data);
                })
            });
        });

    app.get("/api/viewthumb/:file_id", (req, res) => {
        chat.filesRef.child(req.params.file_id).once("value", snapshot => {
            let filedata = snapshot.val();
            let path = `${config.server.resourcespath}${filedata.room_id}/thumb_${snapshot.key}`;

            fs.readFile(path, (err, data) => {
                if (err) {
                    console.error(`API: file ${req.params.file_id} not found`);
                    res.status(404);
                    return;
                }
                res.setHeader('Content-type', filedata.filetype);
                res.send(data)
            })
        });
    });

    app.get("/api/file/:file_id", (req, res) => {
        chat.filesRef.child(req.params.file_id).once("value", snapshot => {
            let filedata = snapshot.val();
            if (filedata) {
                res.json(filedata);
            } else {
                console.error(`API: file ${req.params.file_id} not found`)
                res.status(404);
            }
        });
    });

    app.post("/api/rooms/:room_id/files", validate, (req, res) => {
        let jsonString = '';
        req.on('data', function (data) {
            jsonString += data;
        });

        req.on('end', function () {
            let sentfiles_data = JSON.parse(jsonString),
                promises = [],
                promisesdata = [];

            sentfiles_data.forEach(sentfile_data => {

                let deferred = Q.defer(),
                    buffer = new Buffer(sentfile_data.base_64.split('base64,')[1], "base64");

                if (buffer.byteLength / 1024 > config.server.maxfilesize * 1024) {
                    deferred.resolve();
                    return;
                }

                let filetype;
                try{
                    filetype = fileType(buffer).mime;
                }catch (e) {
                    if (sentfile_data.filename.includes(".txt")) {
                        filetype = "text/plain"
                    }else {
                        console.error("File is not supported");
                        res
                            .status(500)
                            .json({message: "Internal server error"});
                        return;
                    }
                }

                chat.add_file({
                    room_id: req.params.room_id,
                    filename: sentfile_data.filename,
                    filetype: filetype
                }, data => {
                    let path = `${config.server.resourcespath}${req.params.room_id}/${data}`;
                    writeFile(path, buffer, err => {
                        if (err) {
                            console.error("API: error while file saving on fs");
                            console.error(err);
                        }
                        else {
                            //if image, try to generate thumb
                            if (filetype.indexOf("image") === 0) {
                                easyimg.thumbnail({
                                    src: path,
                                    dst: `${config.server.resourcespath}${req.params.room_id}/thumb_${data}`,
                                    width: 160
                                }).then(file => {
                                    console.error("API: thumb generated");
                                }, err => {
                                    console.error("API: error on thumb generation");
                                    console.error(err);
                                })
                            }
                            deferred.resolve();
                            promisesdata.push(data)
                        }
                    });

                });

                promises.push(deferred.promise);
            });
            Q.all(promises).then(() => {
                if (promisesdata.length > 0)
                    chat.add_message_room(req.params.room_id, req.user_id, promisesdata, "file_added");
            })
        });

        res.json({});
    }),

        app.get("/api/", (req, res) => {
            res.json({msg: "Server on"})
        }),

        app.get("/api/config", (req, res) => {
            res.json({
                maxfilesize: config.server.maxfilesize
            });
        }),

        app.get("/api/build", (req, res) => {
            fs.readFile("version.txt", 'utf8', (err, data) => {
                res.send(data)
            })
        })
};

const writeFile = (path, contents, cb) => {
    mkdirp(getDirName(path), err => {
        if (err) return cb(err);
        fs.writeFile(path, contents, cb);
    });
};
