class Chat {

    constructor() {
        this.connections = new Map();

        this.user_connections = new Map();

        this.proxy_user = this.constructor.proxy_user;
        this.proxy_room = this.constructor.proxy_room;

        this.broadcaster = new Broadcaster();

        let serviceaccount = require(config.database.serviceAccount);

        const tunnel = require('tunnel2');

        let proxyAgent


        if(config.server.useProxy){
            proxyAgent = tunnel.httpOverHttp({
                proxy: {
                    host: config.server.proxyHost,
                    port: config.server.proxyPort,
                    ca: config.server.caFile ? [fs.readFileSync(config.server.caFile)] : []
                }
            })
        }

        admin.initializeApp({
            credential: admin.credential.cert(serviceaccount),
            databaseURL: config.database.databaseURL,
            httpAgent: proxyAgent ? proxyAgent : false
        });


        this.db = admin.database().ref(config.database.endpoint);
        // firebase.initializeApp(config.database).database().ref(config.database.endpoint);

        this.onlineUsersRef = this.db.child('online_users');
        this.contactsRef = this.db.child('contacts');
        this.roomsRef = this.db.child('rooms');
        this.historyRef = this.db.child('history');
        this.historyRoomRef = this.db.child('history_room');
        this.usersRef = this.db.child('users');
        this.userStatusRef = this.db.child('user_status');
        this.filesRef = this.db.child('files');
        this.tokensRef = this.db.child('tokens');
        this.unreadRef = this.db.child("unread");

        this.onlineUsersRef.remove();
        this.tokensRef.remove();


        db_events_handlers(this);
        this.Room = Room;

    }

    static proxy_room(data) {
        return {
            id: data.id,
            ids: data.ids,
            users: data.users
        }
    }

    static proxy_user(data) {
        return {
            id: data.id,
            preferedname: data.preferedname,
            name: data.name,
            fullname: data.fullname,
            gender: data.gender,
            avatar: data.avatar,
            department: data.department,
            company: data.company,
            branch: data.branch,
            status: data.status ? data.status : null,
            msg_status: data.msg_status ? data.msg_status : "",
            user_status: data.userStatus ? data.userStatus : null 
        }
    }

    add_user(parsed_data, token, status) {

        let deferred = Q.defer();
        let p = this.find_user(parsed_data.id);
        p.then(snapshot => {
            let user = snapshot.val();
            let proxied_data = Chat.proxy_user(parsed_data);
            if (!user) {
                this.set_user(proxied_data);
            } else {
                this.update_user(proxied_data);
            }

            this.tokensRef.child(proxied_data.id).push().set(token, error => {
                if (error) {
                    return;
                }
                this.onlineUsersRef.child(proxied_data.id).set(true);
                deferred.resolve(true);

            });
        })
            .catch(err => {
                console.error("Chat - error find user");
                console.error(err);
            });

        return deferred.promise;

    }

    set_user(user, callback) {
        this.usersRef.child(user.id).set(user, error => {
            if (callback)
                callback(error);
        });
    }

    update_user(user, callback) {
        this.usersRef.child(user.id).update(user, error => {
            if (callback)
                callback(error);
        });
    }

    add_user_contacts(user_id, contacts_ids) {
        //return this.db.child('contacts').child(user_id).set({ ids: contacts_ids });

        let deferred = Q.defer();
        this.contactsRef.child(user_id).once("value", snapshot => {
            let contacts = snapshot.val() ? snapshot.val().ids : [];
            contacts_ids.forEach(new_contact_id => {
                if (parseInt(new_contact_id) != user_id) {
                    if (contacts.indexOf(parseInt(new_contact_id)) < 0) {
                        contacts.push(parseInt(new_contact_id));
                    }
                }
            })

            this.contactsRef.child(user_id).set({ids: contacts});
            deferred.resolve();
        });

        return deferred.promise;
    }

    get_user_contacts(user, page) {

        let deferred = Q.defer();

        this.onlineUsersRef.once("value", snap_users => {

            //get all online users
            let onlineusers = [];
            snap_users.forEach(u => {
                onlineusers.push(parseInt(u.key));
            });

            this.contactsRef.child(user.id).once("value", snapshot => {

                if (!snapshot.val()) {
                    deferred.resolve([]);
                    return;
                }

                //let ids = snapshot.val().ids.splice(page*10, (page + 1) * 10 ), users = [];
                let ids = snapshot.val().ids,
                    users = [],
                    promises = ids.map(user_id => {

                        let promise_user = this.find_user(user_id);
                        promise_user.then(snapshot => {
                            let user = snapshot.val();
                            user.status = onlineusers && onlineusers.indexOf(user.id) >= 0 ? "online" : "offline";
                            if (user.status == "online") {
                                this.userStatusRef.child(user_id).on("value", userStatusSnapshot => {
                                    let val = userStatusSnapshot.val();
                                    user.msg_status = val;
                                    if(user.userStatus == "0" || user.userStatus == "ATIVO")
                                        users.push(user);
                                })
                            } else {
                                if(user.userStatus == "0" || user.userStatus == "ATIVO")
                                    users.push(user);
                            }
                        });
                        return promise_user;
                    });
                Promise.all(promises).then(() => {
                    users.sort((a, b) => {
                        if (a.preferedname < b.preferedname)
                            return -1;
                        if (a.preferedname > b.preferedname)
                            return 1;
                        return 0;
                    });

                    deferred.resolve(users)
                });
            });
        });

        return deferred.promise;

    }

    validate_token(user_id, token) {
        let deferred = Q.defer();

        this.tokensRef.child(user_id).once("value", snapshot => {
            let tokens = snapshot.val();
            snapshot.forEach(childSnapshot => {
                let key = childSnapshot.key;
                if (childSnapshot.val() == token) {
                    deferred.resolve(true);
                    return;
                }
            });

            deferred.resolve(false);

        });

        return deferred.promise;
    }

    /* find a user by its id */
    find_user(id) {

        return this.usersRef.child(id).once("value");
    }

    /* find a room by its id */
    find_room(id) {
        return this.roomsRef.child(id).once("value");
    }

    /* get user status */
    get_user_status(id) {
        return this.userStatusRef.child(id).once("value");
    }

    /* is contact */
    is_contact(owner_id, check_id) {
        let deferred = Q.defer();
        this.contactsRef.child(owner_id).once("value", snapshot => {
            let data = snapshot.val();

            if (!data) {
                deferred.reject();
                return;
            }

            let ids = data.ids;
            if (ids && ids.indexOf(parseInt(check_id)) >= 0) {
                deferred.resolve();
            }
            else
                deferred.reject();
        })
        return deferred.promise;
    }

    /* get a room and validates presence of user */
    find_room_with_user(room_id, user_id) {

        let deferred = Q.defer();

        this.roomsRef.child(room_id).once("value", snapshot => {

            let temp_room = snapshot.val();
            if (!temp_room || temp_room.ids.indexOf(user_id) < 0)
                deferred.reject();
            else {
                let room = new chat.Room(temp_room.ids);
                room.id = temp_room.id;
                deferred.resolve(room);
            }
        });

        return deferred.promise;
    }

    add_message_room(room_id, sender_id, message, type) {
        let TIMESTAMP = firebase.database.ServerValue.TIMESTAMP;
        let roomref = this.historyRef.push();
        roomref.set({room_id: room_id.toString(), sender_id, message, publish_date: TIMESTAMP, type});
        let ref = this.historyRoomRef.child(room_id).push();
        ref.set({room_id: room_id.toString(), publish_date: TIMESTAMP, type, history_id: roomref.key})
        let thisRoomRef = this.roomsRef.child(room_id);
        thisRoomRef.update({last_iteraction: TIMESTAMP});
    }

    get_user_rooms(user_id) {
        let deferred = Q.defer(),
            rooms = [];
        this.roomsRef.once("value", snapshot => {
            snapshot.forEach(snap_room => {
                let roommodel = snap_room.val();
                if (roommodel.ids.indexOf(user_id) >= 0) {
                    let room = new Room(roommodel.ids);
                    room.id = roommodel.id;
                    room.last_iteraction = roommodel.last_iteraction;
                    rooms.push(room);
                }
            });
            deferred.resolve(rooms);
        })
        return deferred.promise;
    }

    find_room_by_userids(ids) {


        let roomsRef = this.db.child('rooms'), deferred = Q.defer();

        roomsRef.once("value", snapshot => {
            let rooms = snapshot.val();
            //if(!rooms)
            //	deferred.resolve(null)

            let return_room = null;

            snapshot.forEach(roommodel => {
                roommodel = roommodel.val();
                let room = new Room(roommodel.ids);
                room.id = roommodel.id;
                if (room.match_ids(ids))
                    return_room = room;
            })

            if (!return_room) {
                let room = new Room(ids);
                room.save().then(snapshot => {
                    deferred.resolve(room);
                })
            } else {

                deferred.resolve(return_room);
            }
            return;


        });

        return deferred.promise;

    }

    change_user_status(user_id, status) {
        this.find_user(user_id)
            .then(snapshot => {
                let user = snapshot.val();

                this.userStatusRef.child(user.id).set(status)
            });
    }

    add_mutual_contacts(ids) {
        ids.forEach(id => {

            this.contactsRef.child(id).once("value", snapshot => {
                let contacts = snapshot.val() ? snapshot.val().ids : [];
                ids.forEach(new_contact_id => {
                    if (parseInt(new_contact_id) != id) {
                        if (contacts.indexOf(parseInt(new_contact_id)) < 0) {
                            contacts.push(parseInt(new_contact_id));
                        }
                    }
                });
                this.contactsRef.child(id).set({ids: contacts});

            });
        })
    }

    remove_connection(connection) {
        let user = this.find_user(connection.user_id);
    }

    add_file(payload, callback) {
        let fileref = this.filesRef.push();
        return fileref.set({
            room_id: payload.room_id,
            filename: payload.filename,
            filetype: payload.filetype
        }, (data) => {
            if (callback)
                callback(fileref.key);
        });
    }

    logout_user(user_id) {
        this.find_user(user_id)
            .then(snapshot => {
                let user = snapshot.val();

                this.onlineUsersRef.child(user.id).remove(onComplete => {

                })
                chat.user_connections.delete(user_id);
            });

    }

    add_user_room(room_id, user_id) {

        this.find_room(room_id)
            .then(snapshot => {

                let temp_room = snapshot.val();
                //do not allow duplicates
                if (temp_room.ids.indexOf(user_id) >= 0)
                    return false;

                this.find_user(user_id)
                    .then(user_snapshot => {
                        if (user_snapshot.val() != null) {
                            let ids = temp_room.ids, room = new Room(ids);

                            if (temp_room.ids.length != 2) {
                                room.id = temp_room.id;
                            }

                            ids.push(parseInt(user_id));
                            room.ids = ids;

                            room.multiple = true;

                            let p = room.save();
                            chat.add_message_room(room.id, user_id, null, "user_added");

                        }
                    });

            })


    }

    user_leave_room(room_id, user_id) {
        console.log("user leave room")

        this
            .find_room_with_user(room_id, user_id)
            .then(room => {

                let ids = room.ids;
                ids.splice(ids.indexOf(user_id), 1);
                let newroom = new Room(ids);
                newroom.id = room.id;
                newroom.save();

                chat.add_message_room(newroom.id, user_id, null, "user_left");

                this.find_user(user_id)
                    .then(snap_user => {
                        this.broadcaster.user_left_room(newroom, Chat.proxy_user(snap_user.val()));
                    })
            })

    }

}

let chat = new Chat();
