let db_events_handlers = (chat) => {
	chat.onlineUsersRef.on("child_removed", oldChildSnapshot => {
		console.log("Database event: online user removed");
		chat.broadcaster.user_logged_out(oldChildSnapshot.key);
	});
	chat.onlineUsersRef.on("child_added", childSnapshot => {
		console.log("Database event: online user added");
		chat.broadcaster.user_logged_in(childSnapshot.key);
	});
	chat.onlineUsersRef.on("child_changed", childSnapshot => {
		console.log("Database event: online user changed");
		chat.broadcaster.user_logged_in(childSnapshot.key);
	})
	chat.contactsRef.limitToLast(2).on("child_added", childSnapshot => {
		console.log("Database event: user contacts added");
		chat.broadcaster.user_contacts(childSnapshot.key);			
	});
	chat.contactsRef.on("child_changed", childSnapshot => {
		console.log("Database event: user contacts changed");
		chat.broadcaster.user_contacts(childSnapshot.key);			
	});
	chat.contactsRef.on("child_removed", childSnapshot => {
		console.log("Database event: user contacts removed");
		chat.broadcaster.user_contacts(childSnapshot.key);			
	});
	/* user status */
	chat.userStatusRef.on("child_added", childSnapshot => {
		console.log("Database event: user status added");
		chat.broadcaster.user_status(childSnapshot.key, childSnapshot.val());			
	});
	chat.userStatusRef.on("child_changed", childSnapshot => {
		console.log("Database event: user status changed");
		chat.broadcaster.user_status(childSnapshot.key, childSnapshot.val());
	});

	/* rooms */
	chat.roomsRef.limitToLast(1).on("child_added", (childSnapshot, prevChildKey) => {
		console.log("Database event: room added");
		childSnapshot.val().ids.forEach( id => {
			chat.broadcaster.conversations(id);
			chat.broadcaster.get_room(id, childSnapshot.key)
		})
	})
	chat.roomsRef.on("child_changed", (childSnapshot, prevChildKey) => {
		console.log("Database event: room changed");
		chat.broadcaster.conversation(childSnapshot.key, childSnapshot.val());
		childSnapshot.val().ids.forEach( id => {
			//chat.broadcaster.conversations(id);

		})
	});
	chat.roomsRef.on("child_removed", (oldChildSnapshot) => {
		console.log("Database event: room removed");
		//Can a room be removed?!?!
		//oldChildSnapshot.val().ids.forEach( id => {
			//chat.broadcaster.conversations(id);
		//})
	})

	chat.historyRef.limitToLast(1).on("child_added", childSnapshot => {
		console.log("history added")
	})
	chat.historyRef.on("child_changed", (childSnapshot, prevChildKey) => {
		console.log("Database event: history changed");
		let messageobj = childSnapshot.val();
		//console.log(prevChildKey);
		messageobj.history_id = childSnapshot.key;
		chat.broadcaster.message(messageobj)
	});

	chat.historyRef.once('value', histories => {
		//console.log("sadsadsad");
		//console.log(histories);
	})


	chat.usersRef.on("child_added", childSnapshot => {
		
	});

	chat.unreadRef.on("child_added", (childSnapshot, prevChildKey) => {
		console.log("Database event: unread added");
		chat.broadcaster.unread_rooms(childSnapshot.key);
	});
	chat.unreadRef.on("child_changed", (childSnapshot, prevChildKey) => {
		console.log("Database event: unread changed");
		chat.broadcaster.unread_rooms(childSnapshot.key);
	});
	chat.unreadRef.on("child_removed", (oldChildSnapshot) => {
		console.log("Database event: unread removed");
		chat.broadcaster.unread_rooms(oldChildSnapshot.key);
	});
}