
class Room {
	constructor(ids) {
		this.id = shortid.generate();
		this.ids = ids;
		this.history = [];
		this.last_iteraction = null;
	}

	match_ids(to_match_ids) {
		if(to_match_ids.length != this.ids.length)
			return false;
		
		let match = true;
		this.ids.forEach( id => {
			if(to_match_ids.indexOf(id) < 0 )
				match = false;
		});
		if(match) {
			return this;
		}
		
	}

	get_users() {
		return this.ids.map( id => {
			return chat.find_user(parseInt(id));
		})
	}

	build_users() {

		let deferred = Q.defer(), users = [];
		let promises = this.ids.map( user_id => {
			let promise_user = chat.find_user(user_id);
			promise_user.then( snapshot => {
				users.push(Chat.proxy_user(snapshot.val()));
			})
			return promise_user;
		});
		Promise.all(promises).then( () => {
			this.users = users;
			deferred.resolve();
		})
		return deferred.promise;

	}

	add_history(sender, msg, type) {
		this.history.push({
			sender,
			msg,
			type
		})
	}

	add_user(user) {
		if(this.ids.indexOf(user.id)<0) {
			this.ids.push(user.id);
		}
		this.users = this.get_users();
	}

	remove_user(user) {
		if(this.ids.indexOf(user.id)<0) {
			this.ids.splice(this.ids.indexOf(user.id), 1);
		}
		this.users = this.get_users();
	}

	save() {

		let ids = [];
		if(this.ids) {
			ids = this.ids.map( id => {
				return parseInt(id);
			});
		}
		return chat.roomsRef.child(this.id).set({ids: ids, id: this.id });
	}

	get_messages(indexAt) {

		let build_message = (id, history_id) => {
			return chat.historyRef.child(history_id).once("value", snapshot => {
				//messages.push(snapshot.val());
				let m = snapshot.val();
				m.history_id = id;
				chat.find_user(m.sender_id)
				.then( user_snapshot => {
					let sender = user_snapshot.val();
					m.sender = Chat.proxy_user(sender);
					messages.push(m);
				})
			});
		}

		let deferred = Q.defer();
		let messages = [], promises = [];
		if(indexAt) {
			chat.historyRoomRef.child(this.id).orderByKey().endAt(indexAt).limitToLast(5).on("value", historyroom_snap => {
				historyroom_snap.forEach( h => {
					if(h.key != indexAt)
						promises.push(build_message(h.key, h.val().history_id))
				});
				Promise.all(promises).then( () => {
					deferred.resolve(messages);
				})
			});

		} else {
			//get last 5
			chat.historyRoomRef.child(this.id).limitToLast(5).on("value", historyroom_snap => {
				
				historyroom_snap.forEach( h => {
					promises.push(build_message(h.key, h.val().history_id))
				});

				Promise.all(promises).then( () => {
					deferred.resolve(messages);
				})
			});
		}

		return deferred.promise;
	}

}
