class Broadcaster {
	constructor() {

	}

	_sender(conn, data) {
		if(conn.OPEN) {
			try {
				conn.send(JSON.stringify(data));
			} catch(e) {
				console.log("error");
			}
		}
		else {
			console.log("Conn is closed")
			//TODO: remove this connection from the list
		}
	}

	/* TODO: broadcast only to users that logged out is a  contact */
	user_logged_out(user_id) {
		console.log("Broacaster - user_logged_out");

		chat.user_connections.forEach( (connections, key) => {
			if(key != user_id) {
				chat.is_contact(key, user_id)
				.then( () => {
					connections.forEach( conn => {
						this._sender(conn, { type: "user_logged_out", user_id: user_id });
					})
				})
			} else {
				connections.forEach( conn => {
					this._sender(conn, { type: "logged_out" });
				})
			}
		});
	}

	user_logged_in(user_id) {
		console.log("Broacaster - user_logged_in");
		chat.find_user(user_id)
		.then( snapshot => {
			let user = snapshot.val();
			
			chat.user_connections.forEach( (connections, key) => {
				if(key != user_id) {
					chat.is_contact(key, user_id)
					.then( () => {
						connections.forEach( conn => {
							this._sender(conn, { type: "user_got_online", user: chat.proxy_user(user) });
						})
					})
				}
			});

		});
		
	}

	current_user_logged_in(user_id, token) {
		
		console.log("Broacaster - current_user_logged_in: " + user_id);
		chat.find_user(user_id)
		.then( snapshot => {
			let user = snapshot.val();
			chat.user_connections.get(user_id).forEach( (conn) => {
				this._sender(conn, { type: "connected", user: chat.proxy_user(user), token });
			});

			this.unread_rooms(user_id);

		});

		chat.get_user_status(user_id)
		.then( snapshot => {
			let status = snapshot.val();
			this.user_status(user_id, status);
		})
	}


	user_contacts(user_id, page) {
		console.log("Broacaster - user_contacts: " + user_id);
		chat.find_user(user_id)
		.then(snapshot  => {
			
			let user = snapshot.val();
			chat.get_user_contacts(user, page)
			.then( users => {
				let proxed_users = users.map( u => {
					return chat.proxy_user(u);
				})
				chat.user_connections.get(parseInt(user.id)).forEach( conn => {
					this._sender(conn, { type: "got_users", users: proxed_users });
				} )
			})
		})
	}

	user_status(user_id, status) {
		console.log("Broacaster - user_status: " + user_id);

		if(!chat.user_connections.get(parseInt(user_id)))
			return;

		/* to self user */
		chat.user_connections.get(parseInt(user_id)).forEach( conn => {
			this._sender(conn, { type: "status_changed", status }); 
		} )

		/* to other users */
		chat.user_connections.forEach( (connections, key) => {
			if(key != user_id) {
				chat.is_contact(key, user_id)
				.then( () => {
					chat.user_connections.get(parseInt(key)).forEach( conn => {
						this._sender(conn, { type: "user_status_changed", msg_status: status, user_id });
					} );
				})
			}
		});
	}

	get_room(user_id, room_id) {

		chat
		.find_room_with_user(room_id, user_id)
		.then( room => {
			room
			.build_users()
			.then( () => {
				room
				.get_messages(null)
				.then( messages => {
					chat.user_connections.get(user_id).forEach( conn => {
						this._sender(conn, { type: "got_room", room: Chat.proxy_room(room), messages: messages });
					});
				})
			})
			.fail( () => {
				chat.connections.forEach( conn => {
					chat.user_connections.get(user_id).forEach( conn => {
						this._sender(conn, { type: "room_not_found", room_id: room_id });
					})
				})
			} )
		})
	}

	is_typing(user_id, room_id) {

		chat
		.find_room_with_user(room_id, user_id)
		.then( room => {
			room
			.build_users()
			.then( () => {
				room
				.get_messages(null)
				.then( messages => {

					room.ids.forEach( room_user_id => {
						if(room_user_id != user_id) {
							let connections = chat.user_connections.get(parseInt(room_user_id)) || [];
							connections.forEach( conn => {
								this._sender(conn, { type: "user_type", room: room });
							})
						}
					} )

				})
			})
			
		})
	}

	paged_history(room, user_id, first_history_id) {
		room
		.get_messages(first_history_id)
		.then( messages => {

			chat.user_connections.get(user_id).forEach( conn => {
				this._sender(conn, { type: "got_paged_history", room: room, messages: messages });
			})
		})
	}

	conversation(room_id, roomdata) {
		
		chat.find_room(room_id)
		.then(snapshot => {
			let temp_room = snapshot.val();
			let ids = temp_room.ids, room = new Room(ids);
			room.id = room_id;
			room.last_iteraction = temp_room.last_iteraction;

			room.build_users()
			.then( () => {
				ids.forEach( user_id => {
					let connections = chat.user_connections.get(parseInt(user_id)) || [];
					connections.forEach( conn => {
						this._sender(conn, { type: "got_active_conversation", room: room });
					})
				});
			})
		});
	}

	conversations(user_id) {

		chat
		.get_user_rooms(user_id)
		.then( rooms => {
			rooms.forEach( room => {
				room.build_users()
				.then( () => {
					let connections = chat.user_connections.get(parseInt(user_id)) || [];
					connections.forEach( conn => {
						this._sender(conn, { type: "got_active_conversation", room: room });
					})
				})
				
			})
		});
	}

	message(messageobj) {

		console.log("Broacaster - message")
		chat
		.find_room(messageobj.room_id)
		.then( temp_room => {
			let room = new Room(temp_room.val().ids)
			room.id = temp_room.val().id;
			room.build_users()
			.then( () => {

				chat.find_user(messageobj.sender_id)
				.then( user_snapshot => {
					messageobj.sender = Chat.proxy_user(user_snapshot.val());
					room.ids.forEach( user_id => {
						let connections = chat.user_connections.get(parseInt(user_id)) || [];
						connections.forEach( conn => {
							this._sender(conn, { type: "message_sent", room: room, message: messageobj });
						});

						//if(connections.length == 0) {
						chat.unreadRef.child(user_id).child(room.id).set(true);
						//}
					});
				});
			})

			
		})
	}

	user_added_room(room, user) {
		room.build_users()
		.then( () => {
			room.ids.forEach( user_id => {
				let connections = chat.user_connections.get(parseInt(user_id)) || [];
				connections.forEach( conn => {
					this._sender(conn, { type: "user_added_room", room: room });
				})
			} )
		})
	}

	user_left_room(room, user) {

		console.log("Broacaster - user_left_room")

		room.build_users()
		.then( () => {
			room.ids.forEach( user_id => {
				let connections = chat.user_connections.get(parseInt(user_id)) || [];
				connections.forEach( conn => {
					this._sender(conn, { type: "user_left_room", room: room, user: Chat.proxy_user(user) });
				})
			} )
		})

		chat.user_connections.get(parseInt(user.id)).forEach( conn => {
			this._sender(conn, { type: "you_left_room", room: room, user: Chat.proxy_user(user) });
		})
	}

	unread_rooms(user_id) {
		console.log("Broacaster - unread_rooms");
		chat.unreadRef.child(user_id).once("value", snapshot => {
			let connections = chat.user_connections.get(parseInt(user_id)) || [];
			connections.forEach( conn => {
				this._sender(conn, { type: "unread_rooms", room_ids: snapshot.val() });
			});
		});
	}
}
