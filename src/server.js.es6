"use strict";

let express = require('express'),
	http = require('http'),
	jwt = require('jsonwebtoken'),
	ws = require("nodejs-websocket"),
	Q = require('q');

let api_consumer = require('node-mmpapi-consumer').api_consumer;

let config = require('./config.js');
let chat = (require("./chat.js")).chat;

api_consumer.config = config;

let app = express();

app.use( (req, res, next) => {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, logged_user_id, token");
	next();
});



require('./api.js')(app, chat);

app.listen(config.server.api_port);

class Dispatcher {
	
	constructor(conn) {
		this.conn = conn;
	}

	validate(extra) {
		console.log("Dispatcher - validate");

		let this_conn = this.conn;

		api_consumer
		.validate(extra.JSESSIONID)
		.then ( data => {
			this.conn["JSESSIONID"] = extra.JSESSIONID;
			this_conn.user_id = data.id;

			let connections = chat.user_connections.get(data.id) || [];
			connections.push(this_conn);
			chat.user_connections.set(data.id, connections);

			let token = jwt.sign({ id: data.id, name: data.name }, 'ourtoken');
			let p = chat.add_user(data, token, "online");
			p.then( () => {
				chat.broadcaster.current_user_logged_in(data.id, token);
			});
			
		})
		.fail( error => {
			console.error("Error - Dispatcher - validate", error);
			console.error(error);
			this.conn.send(JSON.stringify({ type: "not_allowed" }));
		})
	}

	get_users(extra) {
		console.log("Dispatcher - get_users");
		api_consumer
		.user_contacts(this.conn["JSESSIONID"])
		.then( data => {
			let contacts_ids = [], promises = [];
			let contacts_deferred = Q.defer();

			chat.find_user(this.conn.user_id).then( snapshot => {
				chat.contactsRef.child(snapshot.val().id).once("value", contacts_snapshot => {
					if(contacts_snapshot.val())
						contacts_deferred.resolve(contacts_snapshot.val().ids);
					else
						contacts_deferred.resolve([]);
				});
			});

			contacts_deferred.promise.then( already_contacts_ids => {

				data.contacts.forEach( contact => {
					let deferred = Q.defer();

					if(already_contacts_ids.indexOf(contact.id)>=0) {
						already_contacts_ids.splice(already_contacts_ids.indexOf(contact.id), 1);
					}

					chat.find_user(contact.id).then( snapshot => {
						let user = snapshot.val();
						if(!user) {
							contact["tokens"] = [];
							chat.set_user(contact, (error) => {
								contacts_ids.push(contact.id);
								deferred.resolve();
							});
						} else {
							chat.update_user(contact, (error) => {
								contacts_ids.push(contact.id);
								deferred.resolve();
							});
						}
					});
					
					promises.push(deferred.promise);
				});

				//update user contacts from chat that are not expresso contacts
				if(already_contacts_ids.length > 0) {
					api_consumer.users_by_ids(this.conn["JSESSIONID"], already_contacts_ids.toString()).then( users => {

						users.forEach( user => {
							if(user && user.id) {

								let deferred = Q.defer();
								chat.update_user(user, (error) => {
									deferred.resolve();
								});
								promises.push(deferred.promise);
							}
						});
						this.proceed_validate(promises, contacts_ids);

					})
					.fail( error => {
						console.error("Dispatcher - get_users contacts error", error);
					})

				} else {
					this.proceed_validate(promises, contacts_ids);
				}

			});
		})
		.fail( error => {
			console.error("Dispatcher - get_users error", error);
		})
	}

	proceed_validate(promises, contacts_ids) {

		Q.all(promises).then( () => {
			let logged_user = null,
				p = chat.find_user(this.conn.user_id).then( snapshot => {
					logged_user = snapshot.val();
					return chat.add_user_contacts(logged_user.id, contacts_ids)
				});
			
			p.then( (snapshot) => {
				chat.broadcaster.user_contacts(logged_user.id, 0);
				chat.broadcaster.conversations(logged_user.id);
			})
		});
	}

	user_searched_and_chat(extra) {
		

		if(extra.id == this.conn.user_id) {
			console.error("Dispatcher - user_searched_and_chat: won't add self as contact");
			return;
		}

		console.log("Dispatcher - user_searched_and_chat");
		chat.find_user(extra.id)
		.then( snapshot => {
			let user = snapshot.val();

			if(!user) {
				api_consumer
				.profile(this.conn["JSESSIONID"], extra.id)
				.then( response => {
					chat.set_user(response, (error) => {
						this.chat_with({ ids: [extra.id] })
					})
				})
				.fail( err => {
					console.error("Dispatcher - user profile with id ${extra.id} not found", err)
				})
			} else {
				this.chat_with({ ids: [extra.id] })
			}
		});
	}

	chat_with(extra) {
		console.log("Dispatcher - chat_with");

		let ids = extra.ids;
		ids.push(this.conn.user_id.toString());
		ids = ids.map( id => {
			return parseInt(id);
		})
		chat.find_room_by_userids(ids)
		.then( room => {
			
			room.build_users()
			.then( () => {
				
				let p = room.get_messages(null);

				p.then( messages => {

					this.conn.send(JSON.stringify({ type: "got_room", room: chat.proxy_room(room), messages: messages }));
				})
			})
		});

		chat.add_mutual_contacts(extra.ids)
		
	}

	send_message(extra) {
		console.log("Dispatcher - send_message");
		if(extra.msg.trim().length <= 0 ){
			return false;
		}
		chat.add_message_room(extra.room_id, extra.sender_id, extra.msg, "conversation");
	}

	add_user_room(extra) {
		//TODO: check if connected user can manage this room
		console.log("Dispatcher - add_user_room")
		chat.add_user_room(extra.room_id, extra.user_id);
	}

	get_current_rooms(extra) {
		console.log("Dispatcher - get_current_rooms")
		extra.ids.forEach(room_id => {
			chat.broadcaster.get_room(this.conn.user_id, room_id);
		})
	}

	get_room(extra) {
		console.log("Dispatcher - get_room");
		chat.broadcaster.get_room(this.conn.user_id, extra.id);
	}

	user_typing(extra) {
		console.log("Dispatcher - user_typing");
		chat.broadcaster.is_typing(this.conn.user_id, extra.id);	
	}

	change_status(extra) {
		console.log("Dispatcher - change_status");
		chat.change_user_status(this.conn.user_id, extra.status)
	}

	get_page_history(extra) {
		console.log("Dispatcher - get_page_history");

		chat
		.find_room_with_user(extra.id, this.conn.user_id)
		.then( room => {
			room.build_users()
			.then( () => {
				chat.broadcaster.paged_history(room, this.conn.user_id, extra.history_id);
			})
		})
		.fail( () => {
			conn.send(JSON.stringify({ type: "room_not_found", room_id: extra.id }));
		})

	}

	leave_room(extra) {
		console.log("Dispatcher - leave_room");
		chat.user_leave_room(extra.room_id, this.conn.user_id);
	}

	logout() {
		console.log("Dispatcher - logout");
		chat.logout_user(this.conn.user_id);
		/*
		chat.logout_user(this.conn.user_id);

		this.conn.send(JSON.stringify({ type: "logged_out" }));
		chat.user_connections.delete(this.conn.user_id)
		*/
	}

	room_read(extra) {
		console.log("Dispatcher - room read");
		chat.unreadRef.child(this.conn.user_id).child(extra.room_id).remove();
	}
	
}

ws.createServer( conn => {
    
   	let actions = new Dispatcher(conn);

    conn.on("text", function (data) {
    	data = JSON.parse(data);
    	
    	let can_access = true;
    	if(data.action == "validate") {
    		actions["validate"](data.extra);
    	} else {

			chat.validate_token(conn.user_id, data.token)
			.then( can_access => {

				if(can_access && actions[data.action]) {
					actions[data.action](data.extra)
				} else {
					conn.close();
				}
			})
    		
    	}

    })
    
    conn.on("close", (code, reason) => {
    	console.log("Socket closed");

    	let connections = chat.user_connections.get(conn.user_id) || [],
    		new_connections = [];
    	connections.forEach( connection => {
    		if(connection != conn) {
    			new_connections.push(conn);
    		}
    		chat.user_connections.set(conn.user_id, new_connections);
    	})
		//if(conn.user_id) {
		//	chat.logout_user(conn.user_id);
		//}

    })

    conn.on("error", error => {
		//console.log("Caught flash policy server socket error");
    })
}).listen(config.server.socket_port);
