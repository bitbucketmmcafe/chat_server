#!/bin/bash

source version.sh

mv package.json dist/
mv config.js.sample dist/

cd dist/

zip -r ../chat_server-$VERSION *

cd ..
mv dist/package.json ./
mv dist/config.js.sample ./
