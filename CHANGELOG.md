
#### [Current]
 * [d7102be](../../commit/d7102be) - __(Christian Benseler)__ Dependencies update
 * [c03ee68](../../commit/c03ee68) - __(Christian Benseler)__ readme.md
 * [b2fe4e6](../../commit/b2fe4e6) - __(Christian Benseler)__ Using Firebase 3 API/sdk
 * [8c7d9ed](../../commit/8c7d9ed) - __(Christian Benseler)__ Eu-46
 * [77f5463](../../commit/77f5463) - __(Christian Benseler)__ Server prepared to reverse proxy
 * [3f2ed01](../../commit/3f2ed01) - __(Christian Benseler)__ Eu-47
 * [b6f9909](../../commit/b6f9909) - __(Christian Benseler)__ API: fix file
 * [a9b61bd](../../commit/a9b61bd) - __(Christian Benseler)__ CHanged lib to detect mimetype
 * [3b281ff](../../commit/3b281ff) - __(Christian Benseler)__ Eu-45
 * [0911ab9](../../commit/0911ab9) - __(Christian Benseler)__ Eu-45
 * [9dc5c92](../../commit/9dc5c92) - __(Christian Benseler)__ Eu-30 - some tests
 * [7e3fbb5](../../commit/7e3fbb5) - __(Christian Benseler)__ Several fixes
 * [7e68d02](../../commit/7e68d02) - __(Christian Benseler)__ Eu-41
 * [5e7f65d](../../commit/5e7f65d) - __(Christian Benseler)__ When socket is closed, it's removed from the poll
 * [eff5d4c](../../commit/eff5d4c) - __(Christian Benseler)__ Chat: fix page reload
 * [7fa1547](../../commit/7fa1547) - __(Christian Benseler)__ Eu-38
 * [9037947](../../commit/9037947) - __(Christian Benseler)__ Eu-37
 * [7d9d216](../../commit/7d9d216) - __(Christian Benseler)__ Create version
 * [d2540ed](../../commit/d2540ed) - __(Christian Benseler)__ Eu-36
 * [aa59ccf](../../commit/aa59ccf) - __(Christian Benseler)__ Eu-33 / Eu-34
 * [1f73c22](../../commit/1f73c22) - __(Christian Benseler)__ Multiple chat
 * [2135bea](../../commit/2135bea) - __(Christian Benseler)__ Contacts: pagination
 * [c9906b9](../../commit/c9906b9) - __(Christian Benseler)__ Some fixes
 * [de3ea50](../../commit/de3ea50) - __(Christian Benseler)__ fixes
 * [fd55c38](../../commit/fd55c38) - __(Christian Benseler)__ Broadcaster: sender wrapper
 * [79e0fbb](../../commit/79e0fbb) - __(Christian Benseler)__ Update user data when log in
 * [293c99e](../../commit/293c99e) - __(Christian Benseler)__ Eu-28
 * [b5da7bd](../../commit/b5da7bd) - __(Christian Benseler)__ fix - check if room exists before get ids
 * [3c5f2de](../../commit/3c5f2de) - __(Christian Benseler)__ Eu-4 - initial implementation
 * [2afa55c](../../commit/2afa55c) - __(Christian Benseler)__ Eu-26: dispatcher and broadcaster for user status
 * [05d2dd4](../../commit/05d2dd4) - __(Christian Benseler)__ user tokens stored in own nodes
 * [0555901](../../commit/0555901) - __(Christian Benseler)__ Readme and some console.error
 * [a0f3692](../../commit/a0f3692) - __(Christian Benseler)__ Eu-23: search user with API, add do db and then to user contacts
 * [25d8a9b](../../commit/25d8a9b) - __(Andre)__ create build sh
 * [304a9a5](../../commit/304a9a5) - __(Christian Benseler)__ Eu-6: save thumb
 * [d712ad9](../../commit/d712ad9) - __(Christian Benseler)__ Added contenttype to response file
 * [24b4a8d](../../commit/24b4a8d) - __(Christian Benseler)__ Eu-17
 * [bb2e3cc](../../commit/bb2e3cc) - __(Christian Benseler)__ Eu-16 / Eu6 -  API que recebe e disponibiliza arquivo de uma conversa
 * [c79ca50](../../commit/c79ca50) - __(Christian Benseler)__ Dispatch error on validate
 * [5c0a5e2](../../commit/5c0a5e2) - __(Christian Benseler)__ package.json info and changelog generation

#### v0.0.1
 * [e7ab217](../../commit/e7ab217) - __(Christian Benseler)__ Eu-3 - database auth
 * [e9d6b55](../../commit/e9d6b55) - __(Christian Benseler)__ apiconsumer as module
 * [9d81843](../../commit/9d81843) - __(Christian Benseler)__ fixes
 * [834f1d3](../../commit/834f1d3) - __(Christian Benseler)__ new structure src/dist
 * [e5b09a2](../../commit/e5b09a2) - __(Christian Benseler)__ more readme
 * [83a5798](../../commit/83a5798) - __(Christian Benseler)__ new structure + folder dist
 * [c7c503f](../../commit/c7c503f) - __(Christian Benseler)__ readme
 * [f549b94](../../commit/f549b94) - __(Christian Benseler)__ api
 * [2046b5e](../../commit/2046b5e) - __(Christian Benseler)__ config
 * [584e815](../../commit/584e815) - __(Christian Benseler)__ api
 * [c860f66](../../commit/c860f66) - __(Christian Benseler)__ dispatcher and api consumer
 * [77c74e6](../../commit/77c74e6) - __(Christian Benseler)__ major structure changes
 * [b11d60c](../../commit/b11d60c) - __(Christian Benseler)__ load previous chat msgs
 * [63e9d66](../../commit/63e9d66) - __(Christian Benseler)__ chat: load previous
 * [23ba682](../../commit/23ba682) - __(Christian Benseler)__ add/remove user from chat fix
 * [ded51c6](../../commit/ded51c6) - __(Christian Benseler)__ check if conn exists
 * [deece67](../../commit/deece67) - __(Christian Benseler)__ fix login/out!
 * [2681769](../../commit/2681769) - __(Christian Benseler)__ fix
 * [4502b57](../../commit/4502b57) - __(Christian Benseler)__ config file
 * [bdf7d7c](../../commit/bdf7d7c) - __(Christian Benseler)__ config file
 * [9436b8c](../../commit/9436b8c) - __(Christian Benseler)__ major fixes
 * [5bb1b87](../../commit/5bb1b87) - __(Christian Benseler)__ major changes structure
 * [0311f27](../../commit/0311f27) - __(Christian Benseler)__ major implementations
 * [9c98589](../../commit/9c98589) - __(Christian Benseler)__ first commit
