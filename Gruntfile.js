module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		
		watch: {
			
			chatbundle: {
				files: ["src/chat/_*.js.es6"],
				tasks: ["concat:chatbundle"]
			},
			dist: {
				files: ['src/api.js.es6', 'src/apiconsumer.js.es6', 'src/server.js.es6'],
				tasks: ['copy:main']
			}
			
		},

		concat: {
			chatbundle: {
				src: ["src/chat/_imports.js.es6",
					  "src/chat/_room.js.es6",
					  "src/chat/_broadcast.js.es6",
					  "src/chat/_dbevents.js.es6",
					  "src/chat/_chat.js.es6",
					  "src/chat/_export.js.es6"],
				dest: "dist/chat.js"
			}
		},

		copy: {

			main: {
				files: [{
					expand: true,
					flatten: true,
					src: ["src/api.js.es6",
						"src/apiconsumer.js.es6",
						"src/server.js.es6"],
					dest: "dist/",
					rename: function(dest, src) {
						return dest + src.replace(/\.js.es6$/, ".js");
					}
				}]
			}
		}

	});

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-exec');

  // Default task(s).
  grunt.registerTask('default', ['concat:chatbundle', 'copy:main', 'watch']);

};
